$(function() {
	'use strict';

	var isMobile,
		isDesktop,

		_sort = sort.js,
		$gView = $('html, body'),
		$lHeader = $('.lheader');

	//#region    [!]----------[ BASE ]----------[!]

	//#region [hint]
	var isTooltipOpen = !1;

	$('.js_hint')
		.tooltipster({
			delay: 200,
			side: ['top'],
			trigger: 'custom',
			animation: 'anim',
			animationDuration: 200
		})
		.on('mouseenter', function() {
			if (!isTooltipOpen) {
				isTooltipOpen = true;
				$(this).tooltipster('open');
			}
		})
		.on('mouseleave', function() {
			if (isTooltipOpen) {
				isTooltipOpen = false;
				$(this).tooltipster('hide');
			}
		});
	//#endregion [hint]

	//#region [hero]
	$('.hero').addClass('sm_item_show');
	//#endregion [hero]

	//#region [endless]
	endlessEnable();

	function endlessEnable() {
		var $box = $('.js_endless');

		if (!$box.hasClass('js_endless_on')) {
			$box
				.addClass('js_endless_on')
				.infiniteScroll({
					history: false,
					status: '.loader',
					path: '.js_endless_next',
					append: '.js_endless_item',
				});
		}
	}
	function endlessDisable() {
		var $box = $('.js_endless');

		if ($box.hasClass('js_endless_on')) {
			$box
				.removeClass('js_endless_on')
				.infiniteScroll('destroy');
		}
	}
	//#endregion [endless]

	//#endregion [!]----------[ BASE ]----------[!]

	//#region    [!]----------[ MENU ]----------[!]

	//#region [mmain]
	var mMainTimeout;

	function mMainClean() {
		clearTimeout(mMainTimeout);
		mMainTimeout = setTimeout(function() {
			$('.mmain_ul2, .mmain_lnk1')
				.removeAttr('style')
				.removeClass('_open');
		}, 300);
	}
	function mMainClose($lnk, $sub) {
		$lnk = $lnk ? $lnk : $('.mmain_lnk1._open');

		if ($lnk.length) {
			$sub = $sub ? $sub : $lnk.next();

			$lnk.removeClass('_open');
			$sub.stop()
				.slideUp(300, 'easeInOutSine', function() {
					$(this)
						.removeAttr('style')
						.removeClass('_open');
				});
		}
	}

	$('.js_open_sub').on('click', function() {
		if (isMobile) {
			var $lnk = $(this),
				$sub = $lnk.next();

			if ($lnk.hasClass('_open')) {
				mMainClose($lnk, $sub);
			} else {
				mMainClose();

				$lnk.addClass('_open');
				$sub.stop()
					.slideDown(300, 'easeInOutSine', function() {
						$(this)
							.removeAttr('style')
							.addClass('_open');
					});
			}

			return false;
		}
	});
	//#endregion [mmain]

	//#region [mnews]
	var $mnewsMore = $('.mnews_more');
	$('.mnews_btn').on('click', function() {
		$(this).add($mnewsMore).toggleClass('_active');
	});
	//#endregion [mnews]

	//#region [mshare]
	var Share = {
		vk: function() {
			var o = this,
				url = 'https://vk.com/share.php?url=';

			url += o.url();
			o.popup(url);
		},
		ok: function() {
			var o = this,
				url = 'https://connect.ok.ru/dk?cmd=WidgetSharePreview&st.cmd=WidgetSharePreview&st._aid=ExternalShareWidget_SharePreview&st.shareUrl=';

			url += o.url();
			o.popup(url);
		},
		fb: function() {
			var o = this,
				url = 'https://www.facebook.com/sharer/sharer.php?u=';

			url += o.url();
			o.popup(url);
		},
		tw: function() {
			var o = this,
				url = 'https://twitter.com/intent/tweet?url=';

			url += o.url();
			o.popup(url);
		},
		url: function() {
			var href = location.href,
				url = encodeURIComponent(href);
			return url;
		},
		popup: function(url) {
			window.open(url, '_blank');
		}
	};

	$('.mshare_link').on('click', function() {
		var $this = $(this),
			type = $this.data('type');

		switch(type) {
			case 'vk': Share.vk(); break;
			case 'fb': Share.fb(); break;
			case 'tw': Share.tw(); break;
			case 'ok': Share.ok(); break;
		} return false;
	});
	//#endregion [mshare]

	//#region [mmobile]
	var $mMobileBox = $('.mmobile_box'),
		$mMobileBtn = $('.mmobile_btn');

	function mMobileClose() {
		mMainClean();

		$mMobileBtn.removeClass('_active');
		$mMobileBox.slideUp(300, 'easeInOutSine', function() {
			$(this)
				.removeAttr('style')
				.removeClass('_active');
		});
	}

	$(document).mouseup(function(e) {
		if (
			isMobile &&
			!$lHeader.is(e.target) &&
			$lHeader.has(e.target).length === 0
		) {
			mMobileClose();
		}
	});
	$('.mmobile_btn').on('click', function() {
		var $this = $(this);

		if ($this.hasClass('_active')) {
			mMobileClose();
		} else {
			var index = $this.data('index'),
				$box = $mMobileBox.eq(index);

			$mMobileBtn.addClass('_active');
			$box.slideDown(300, 'easeInOutSine', function() {
				$box
					.addClass('_active')
					.removeAttr('style');
			});
		}
	});
	//#endregion [mmobile]

	//#endregion [!]----------[ MENU ]----------[!]

	//#region    [!]----------[ SHOW ]----------[!]

	//#region [smain]
	new Swiper('.smain', {
		loop: true,
		speed: 400,
		effect: 'fade',
		simulateTouch: false,
		on: {
			slideChange: function () {
				var index = this.activeIndex,
					$item = $(this.slides[index]);

				$item.addClass('sm_item_show').siblings().removeClass('sm_item_show');
			}
		},
		autoplay: {
			delay: 5000
		},
		pagination: {
			el: '.smain_dots'
		},
		navigation: {
			nextEl: '.smain_prev',
			prevEl: '.smain_next'
		}
	});
	//#endregion [smain]

	//#region [snews]
	new Swiper('.snews', {
		spaceBetween: 20,
		slidesPerView: 'auto',
		breakpoints: {
			1023: {
				spaceBetween: 0
			}
		}
	});
	//#endregion [snews]

	//#region [simage]
	new Swiper('.simage_show', {
		loop: true,
		grabCursor: true,
		autoplay: {
			delay: 5000
		},
		navigation: {
			nextEl: '.simage_prev',
			prevEl: '.simage_next'
		}
	});
	//#endregion [simage]

	//#region [sgoods]
	new Swiper('.sgoods_show', {
		loop: true,
		grabCursor: true,
		autoplay: {
			delay: 5000
		},
		navigation: {
			nextEl: '.sgoods_prev',
			prevEl: '.sgoods_next'
		}
	});
	//#endregion [sgoods]

	//#endregion [!]----------[ SHOW ]----------[!]

	//#region    [!]----------[ TABS ]----------[!]

	//#region [tuser]
	$('.tuser_link').on('click', function() {
		var $this = $(this),
		index = $this.parent().index(),
		$item = $('.tuser_item').eq(index);

		$item.addClass('_active').siblings().removeClass('_active');
		$('.tuser_link').removeClass('_active');
		$this.addClass('_active');
	});
	//#endregion [tuser]

	//#endregion [!]----------[ TABS ]----------[!]

	//#region    [!]----------[ POPUP ]----------[!]
	var $pMsg = $('.js_pmsg').remodal(),
		$pBuy = $('.js_pbuy').remodal(),
		$pUser = $('.js_puser').remodal();

	//#region [pmsg]
	function pMsgShow(message) {
		if (message) {
			$('.pmain_message').html(message);
			$pMsg.open();
		}
	}
	//#endregion [pmsg]

	//#region [pbuy]
	$(document).on('click', '.shop_buy', function() {
		var $this = $(this),
			img = $this.parent().find('.shop_img').attr('src'),
			url = $this.attr('href');

		$('.js_pbuy_fail').addClass('_active').siblings().removeClass('_active');

		$.ajax({
			url: url,
			type: 'post',
			complete: function() {
				$pBuy.open();
			},
			success: function(data) {
				if (data.status === 'success') {
					$('.js_pbuy_img').attr('src', img);
					$('.js_pbuy_done').addClass('_active').siblings().removeClass('_active');
				}
			}
		});

		return false;
	});
	//#endregion [pbuy]

	//#region [puser]
	$('.js_login_btn').on('click', function() {
		$('.js_puser_tab:eq(0)').addClass('_active').siblings().removeClass('_active');

		$pUser.open();
		return false;
	});
	$('.pmain_form_switch').on('click', function() {
		var $prnt = $(this).closest('.js_puser_tab');

		$prnt.removeClass('_active').siblings().addClass('_active');
	});
	//#endregion [puser]

	//#endregion [!]----------[ POPUP ]----------[!]

	//#region    [!]----------[ FORM CONF ]----------[!]

	//#region [fn]
	function fnFormReset($form) {
		$form[0].reset();
		$form.find('input').iCheck('update');
		$form.find('.form_file_btn').html('');
		$form.find('select').selectOrDie('update');
	}
	function fnFormSubmit($form, doneCallback, failCallback) {
		var url = $form.attr('action'),
			enctype = $form.attr('enctype'),
			$button = $form.find(':submit'),
			ajaxConfig = {},
			data;

		var fnFail = function(data) {
			if (typeof(failCallback) === 'function') {
				failCallback(data);
			}
		};

		$button.prop('disabled', true);

		if (enctype !== 'multipart/form-data') {
			data = $form.serialize();
		} else {
			data = new FormData($form[0]);

			ajaxConfig = {
				cache: false,
				timeout: 600000,
				processData: false,
				contentType: false
			};
		}

		$.extend(ajaxConfig, {
			url: url,
			data: data,
			type: 'post',
			dataType: 'json',
			error: function(data) {
				fnFail(data);
			},
			success: function(data) {
				pMsgShow(data.message);

				if (data.success === 'Y') {
					fnFormReset($form);

					if (typeof(doneCallback) === 'function') {
						doneCallback(data);
					}
				} else {
					fnFail(data);
				}
			},
			complete: function() {
				$button.prop('disabled', false);
			}
		});

		$.ajax(ajaxConfig);
	}
	function fnFormValidate(formName, doneCallback, failCallback) {
		var $form = $('#' + formName);

		$form.validate({
			errorPlacement: function() {
				return false;
			},
			submitHandler: function() {
				fnFormSubmit($form, doneCallback, failCallback);
			}
		});
	}
	//#endregion [fn]

	//#region [config]
	$.validator.setDefaults({
		errorClass: '_error',
		validClass: '_valid',

		rules: {
			/*hiddenRecaptcha: {
				required: function() {
					if (grecaptcha.getResponse() === '') {
						return true;
					} else {
						return false;
					}
				}
			}*/
		},
		highlight: function(element, errorClass, validClass) {
			var type = element.type,
				$this = $(element),
				$item = $this;

			if (type === 'select-one') {
				$item = $this.parent('.sod_select');
			} else
			if (type === 'radio' || type === 'checkbox') {
				$item = this.findByName(element.name).parent('.radio, .checkbox');
			}

			$item.add($this).addClass(errorClass).removeClass(validClass);
		},
		unhighlight: function(element, errorClass, validClass) {
			var type = element.type,
				$this = $(element),
				$item = $this;

			if (type === 'select-one') {
				$item = $this.parent('.sod_select');
			} else
			if (type === 'radio' || type === 'checkbox') {
				$item = this.findByName(element.name).parent('.radio, .checkbox');
			}

			$item.add($this).addClass(validClass).removeClass(errorClass);
		}
	});

	$.validator.addMethod('r_age', function() {
		var check = false,
			value = $('#birthday').val(),
			thisDate, pickDate, diffDate;

		if (value) {
			thisDate = moment();
			pickDate = moment(value, 'DD-MM-YYYY');
			diffDate = thisDate.diff(pickDate, 'years', true);

			if (diffDate >= 18) {
				check = true;
			}
		} return check;
	}, 'Only adults are allowed');
	$.validator.addMethod('r_ltrs', function(value, element) {
		return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z\s]+$/i.test(value);
	}, 'Only letters are allowed');
	$.validator.addMethod('r_alnm', function(value, element) {
		return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z0-9\s]+$/i.test(value);
	}, 'Only letters and digits are allowed');
	$.validator.addMethod('r_text', function(value, element) {
		return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z0-9\s\.,\-\(\)]+$/i.test(value);
	}, 'Only letters, nembers and punctuation are allowed');
	$.validator.addMethod('r_file', function(value, element, param) {
		param = typeof param === 'string' ? param.replace(/,/g, '|') : 'png|jpe?g|gif|zip|pdf|doc?x';
		return this.optional(element) || value.match(new RegExp('\\.(' + param + ')$', 'i'));
	}, 'Please enter a value with a valid extension.');

	$.validator.addMethod('r_date', function(value, element) {
		return this.optional(element) || /(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20)\d\d/i.test(value);
	}, 'Incorrect date');
	$.validator.addMethod('r_phone', function(value, element) {
		// +0 (000) 000-00-00 || +000 (00) 000-00-00
		return this.optional(element) || /^\+?(\d{3}|\d{1})\s?\(?(\d{3}|\d{2})\)?\s?\d{3}(-|\s)?\d{2}(-|\s)?\d{2}$/i.test(value);
	}, 'Incorrect phone');

	$.validator.addClassRules('v_url', {url: true});
	$.validator.addClassRules('v_age', {r_age: true});
	$.validator.addClassRules('v_ltrs', {r_ltrs: true});
	$.validator.addClassRules('v_alnm', {r_alnm: true});
	$.validator.addClassRules('v_text', {r_text: true});
	$.validator.addClassRules('v_file', {r_file: true});
	$.validator.addClassRules('v_dgts', {digits: true});
	$.validator.addClassRules('v_phone', {r_phone: true});
	//#endregion [config]

	//#endregion [!]----------[ FORM CONF ]----------[!]

	//#region    [!]----------[ FORM FLDS ]----------[!]

	//#region [file]
	$('.js_file').on('change', function() {
		var $this = $(this),
			value = $this.val();

		$this.next().text(
			value.replace(/^.*[\\\/]/, '')
		);
	});
	//#endregion [file]

	//#region [select]
	$('select').selectOrDie();
	$(document).on('change', 'select[required]', function() {
		var $this = $(this),
			check = false;

		if ($this.hasClass('js_bday_itm')) {
			if (this.id === 'birthday_d') {
				check = true;
			}
		} else {
			check = true;
		}

		if (check) {
			$this.valid();
		}
	});
	//#endregion [select]

	//#region [t_phone]
	if ($('.t_phone').length) {
		$('.t_phone').mask('+375 (00) 000-00-00', {placeholder: '+375 (__) ___-__-__'});
	}
	//#endregion [t_phone]

	//#region [radio checkbox]
	$('input:not(.fpts_radio):not(.ways_flag)').iCheck({
		labelHover: false,

		radioClass: 'radio',
		hoverClass: '_hover',
		checkedClass: '_checked',
		checkboxClass: 'checkbox',
		labelHoverClass: '_hover'
	});
	$(document).on('ifChecked', 'input[required]', function() {
		$(this).valid();
	});
	//#endregion [radio checkbox]

	//#endregion [!]----------[ FORM FLDS ]----------[!]

	//#region    [!]----------[ FORM LIST ]----------[!]
	fnFormValidate('fmsg');
	fnFormValidate('fremind');

	//#region [fpts]
	var fptsTimeout;
	$('.fpts_radio').on('change', function() {
		var $this = $(this),
			$item = $this.parent();

		if ($this.is(':checked')) {
			$item.addClass('_chosen').siblings().removeClass('_chosen');

			clearTimeout(fptsTimeout);
			fptsTimeout = setTimeout(function() {
				fnFormSubmit($('#fpts'));
			}, 600);
		}
	});
	//#endregion [fpts]

	//#region [freg]

	//#region [file]
	if ($('#freg_file').length) {
		var fregCanvas = document.createElement('canvas');
		var fregCanvasCtx = fregCanvas.getContext('2d');

		$('#freg_file').change(function() {
			var file = $(this).get(0).files[0];

			loadImage(
				file,
				function(img) {
					var imgWidth = img.width,
						imgHeight = img.height;

					if (imgWidth >= 230 || imgHeight >= 230) {
						fregCanvas.width = imgWidth;
						fregCanvas.height = imgHeight;
						fregCanvasCtx.drawImage(img, 0, 0);

						var dataURL = fregCanvas.toDataURL();
						$('.freg_photo').css('background-image', 'url('+dataURL+')');
					} else {
						console.log('Wrong image size!');
					}
				},{
					canvas: true,
					minWidth: 230,
					minHeight: 230
				}
			);
		});
	}
	//#endregion [file]

	//#region [kids]
	var kidNum = $('.freg_kids_item').length;

	$('.js_kids_add').on('click', function() {
		$('.freg_kids').append(
			'<div class="freg_kids_item">'+
				'<div class="freg_kids_cell _1">'+
					'<label class="freg_lbl">Имя</label>'+
					'<input class="freg_input input v_ltrs" type="text" name="kid_name['+kidNum+']" required>'+
				'</div><!--[ freg_kids_cell ]-->'+
				'<div class="freg_kids_cell _2">'+
					'<label class="freg_lbl">Возраст</label>'+
					'<input class="freg_input input v_dgts" type="text" name="kid_age['+kidNum+']" required>'+
				'</div><!--[ freg_kids_cell ]-->'+
				'<div class="freg_kids_cell _3">'+
					'<label class="freg_kids_sex">'+
						'<input type="radio" name="sex['+kidNum+']" value="male" required>Мальчик'+
					'</label><!--[ freg_kids_sex ]-->'+
					'<label class="freg_kids_sex">'+
						'<input type="radio" name="sex['+kidNum+']" value="female" required>Девочка'+
					'</label><!--[ freg_kids_sex ]-->'+
				'</div><!--[ freg_kids_cell ]-->'+
				'<button class="freg_kids_btn _rmv js_kids_rmv" type="button"></button>'+
			'</div><!--[ freg_kids_item ]-->'
		);

		$('input').iCheck({
			radioClass: 'radio',
			checkedClass: '_checked',
			checkboxClass: 'checkbox'
		});

		kidNum++;
	});

	$(document).on('click', '.js_kids_rmv', function() {
		$(this).parent().remove();
		kidsNum--;
	});
	//#endregion [kids]

	//#region [birthday]
	(function() {
		if ($('#freg').length) {
			var thisDate,  thisDateY,
				thisDateM, thisDateD,
				pickDate,  pickDateY,
				pickDateM, pickDateD,
				$itemDate,  $itemDateY,
				$itemDateM, $itemDateD,

			thisDate = moment();

			$itemDate  = $('#birthday');
			$itemDateY = $('#birthday_y');
			$itemDateM = $('#birthday_m');
			$itemDateD = $('#birthday_d');

			thisDateY = thisDate.year();
			thisDateD = thisDate.date();
			thisDateM = thisDate.month();

			var fillItemM = function(max) {
				var month,
					monthNum,
					monthName,
					monthList = '<option value="">Месяц</option>';

				for (var i = 0; i <= max; i++) {
					month = thisDate.month(i);
					monthNum = month.format('MM');
					monthName = month.format('MMMM');
					monthList += '<option value="'+monthNum+'">'+monthName+'</option>';
				}

				$itemDateM
					.html(monthList)
					.selectOrDie('enable')
					.selectOrDie('update');

				$itemDateD
					.html('<option value="">Число</option>')
					.selectOrDie('disable')
					.selectOrDie('update')
			}
			var fillItemD = function(max) {
				var day,
					dayList = '<option value="">Число</option>';

				for (var i = 1; i <= max; i++) {
					day = (i < 10) ? '0' + i: i;
					dayList += '<option value="'+day+'">'+day+'</option>';
				}

				$itemDateD
					.html(dayList)
					.selectOrDie('enable')
					.selectOrDie('update');
			}

			$itemDateY.on('change', function() {
				var numM = 11;

				pickDateY = $(this).val();
				$('.js_bday_itm, .js_bday_box').removeClass('_error');

				if (pickDateY === thisDateY) {
					numM = thisDateM;
				}

				$itemDate.val('');
				fillItemM(numM);
			}).one('change', function() {
				$itemDateY.selectOrDie('disable', 0);
			});
			$itemDateM.on('change', function() {
				var numD = thisDateD;

				pickDateM = $(this).val();
				$('.js_bday_itm, .js_bday_box').removeClass('_error');

				if (pickDateY !== thisDateY) {
					numD = moment().year(pickDateY).month(pickDateM - 1).daysInMonth();
				}

				$itemDateM.selectOrDie('disable', 0);
				$itemDate.val('');
				fillItemD(numD);
			});
			$itemDateD.on('change', function() {
				pickDateD = $(this).val();
				pickDate = pickDateD+'.'+pickDateM+'.'+pickDateY;

				$itemDateD.selectOrDie('disable', 0);
				$itemDate.val(pickDate).valid();
				$('.js_bday_itm').valid();
			});
		}
	})();
	//#endregion [birthday]

	//#endregion [freg]

	//#region [ftest]
	$('.js_ftest_next').on('click', function() {
		var $step = $(this).parent(),
			index = $step.index() + 2;

		$('.htest').addClass('htest-scene2');
		$('.htest_step, .ftest_step').removeClass('_active').filter(':nth-child('+index+')').addClass('_active');
	});
	$('.js_ftest_flag')
		.on('ifChecked', function() {
			var $this = $(this),
				$step = $this.closest('.ftest_step'),
				$item = $this.closest('.ftest_item'),
				$button = $step.find('.ftest_button');

			$item.addClass('ftest_item-active');
			$button.prop('disabled', false);
		})
		.on('ifUnchecked', function() {
			var $this = $(this),
				$item = $this.closest('.ftest_item'),
				$step, $button, $chosen;

			$item.removeClass('ftest_item-active');

			$step = $this.closest('.ftest_step');
			$button = $step.find('.ftest_button');
			$chosen = $step.find('.ftest_item-active');

			if (!$chosen.length) {
				$button.prop('disabled', true);
			}
		});
	//#endregion [ftest]

	//#region [fclubs]
	var fClubsTimeout,
		$clubsEndless = !1;

	clubsEndlessOn();

	$('#fclubs').on('submit', function() {
		getClubs($(this));
		return false;
	});

	$(document).on('click', '.msort_link', function() {
		getClubs($(this), true);
		return false;
	});

	function getClubs($item, isLink) {
		clearTimeout(fClubsTimeout);
		fClubsTimeout = setTimeout(function() {
			clubsEndlessOff();

			var ajaxConfig = {
				type: 'get',
				dataType: 'json',
				success: function(data) {
					var $box = $('.lmore'),
						y = $box.offset().top;

					$gView.animate({
						scrollTop: y
					}, 400);

					$box.html(
						tmpl('tmpl_clubs_base', data)
					);

					if (data.clubs.next) {
						clubsEndlessOn();
					}

					//#region [response]
					/*
					{
						menu: [
							{
								url: '#',
								name: 'дате',
								type: 'date',
								className: '_asc _active'
							},{
								url: '#',
								name: 'популярности',
								type: 'rating',
								className: ''
							}
						],
						clubs: {
							list: [
								{
									url: 'clubs_item.html',
									img: '/assets/img/image/clubs/1_1.jpg',
									text: '<p>гКлуб с комфортными условиями для занятий, продуманными обучающими программами и опытными тренерами.</p>',
									rating: 10
								},{
									url: 'clubs_item.html',
									img: '/assets/img/image/clubs/1_1.jpg',
									text: '<p>гКлуб с комфортными условиями для занятий, продуманными обучающими программами и опытными тренерами.</p>',
									rating: 100
								},{
									url: 'clubs_item.html',
									img: '/assets/img/image/clubs/2_1.jpg',
									text: '<p>вКлуб с комфортными условиями для занятий, продуманными обучающими программами и опытными тренерами.</p>',
									rating: 80
								},{
									url: 'clubs_item.html',
									img: '/assets/img/image/clubs/3_1.jpg',
									text: '<p>бКлуб с комфортными условиями для занятий, продуманными обучающими программами и опытными тренерами.</p>',
									rating: 50
								},{
									url: 'clubs_item.html',
									img: '/assets/img/image/clubs/4_1.jpg',
									text: '<p>аКлуб с комфортными условиями для занятий, продуманными обучающими программами и опытными тренерами.</p>',
									rating: 15
								}
							],
							next: 'N'
						},
						found: 'Y',
						success: 'Y'
					}
					*/
					//#endregion [response]
				}
			};

			if (isLink) {
				ajaxConfig.url = $item.attr('href');
			} else {
				ajaxConfig.url = $item.attr('action');
				ajaxConfig.data = $item.serialize();
			}

			$.ajax(ajaxConfig);
		}, 200);
	}
	function clubsEndlessOn() {
		if ($('.js_endless_next').length) {
			$clubsEndless = $('.clubs').infiniteScroll({
				history: false,
				status: '.loader',
				responseType: 'text',
				path: function() {
					var $link = $('.js_endless_next'),
						href = $link.attr('href');

					if (href) {
						return href;
					} else {
						return false;
					}
				}
			});

			$clubsEndless.on('load.infiniteScroll', function(event, response) {
				var data = JSON.parse(response),
					html = tmpl('tmpl_clubs_list', data);

				$('.js_endless_next').remove();
				$('.clubs').append(html);

				if (!data.clubs.next) {
					clubsEndlessOff();
				}
			});
		}
	}
	function clubsEndlessOff() {
		if ($clubsEndless !== false) {
			$clubsEndless.infiniteScroll('destroy');
			$clubsEndless = false;
		}
	}
	//#endregion [fclubs]

	//#region [freg & flogin]
	$('#freg, #flogin').validate({
		errorPlacement: function() {
			return false;
		},
		submitHandler: function() {
			return true;
		}
	});
	//#endregion [freg & flogin]

	//#endregion [!]----------[ FORM LIST ]----------[!]

	//#region    [!]----------[ RESPONSE ]----------[!]
	var checkScreenSizeON = !1;

	function checkScreenSize() {
		if (window.matchMedia('screen and (max-width: 980px)').matches) {
			if (!isMobile) {
				isMobile = true;
				isDesktop = false;
			}
		} else
		if (!isDesktop) {
			isMobile = false;
			isDesktop = true;
		}

		checkScreenSizeON = !1;
	}

	checkScreenSize();

	$(window).on('resize.check', function() {
		if (!checkScreenSizeON) {
			checkScreenSizeON = true;

			requestAnimationFrame(function() {
				checkScreenSize();
			});
		}
	});
	//#endregion [!]----------[ RESPONSE ]----------[!]

	//#region    [!]----------[ FUNCTION ]----------[!]
	$.extend($.easing, {
		easeInSine: function (e, n, t, a, u) {
			return -a * Math.cos(n / u * (Math.PI / 2)) + a + t
		},
		easeOutSine: function (e, n, t, a, u) {
			return a * Math.sin(n / u * (Math.PI / 2)) + t
		},
		easeInOutSine: function (e, n, t, a, u) {
			return -a / 2 * (Math.cos(Math.PI * n / u) - 1) + t
		}
	});
	//#endregion [!]----------[ FUNCTION ]----------[!]

});

if (window.ymaps) {
	ymaps.ready(mapInit);

	function mapInit() {
		var $map = $('#map'),
			lat = $map.data('lat'),
			lng = $map.data('lng'),
			src = $map.data('src');

		var myMap = new ymaps.Map('map', {
				zoom: 12,
				controls: [],
				scrollZoom: false,
				center: [lat, lng]
			},{
				searchControlProvider: 'yandex#search'
			}),
			objectManager = new ymaps.ObjectManager({
				gridSize: 30,
				clusterize: true,
				clusterDisableClickZoom: true
			});

		myMap.controls.add('zoomControl', {
			size: 'small',
			float: 'none',
			position: {
				top: 0,
				left: 0
			}
		});

		myMap
			.behaviors
			.disable(['scrollZoom']);

		objectManager.objects.options.set({
			iconImageSize: [29, 41],
			iconImageOffset: [-15, -41],
			iconLayout: 'default#image',
			iconImageHref: '/assets/img/design/obj/pin_1.svg'
		});

		objectManager.clusters.options.set({
			preset: 'islands#nightClusterIcons'
		});

		myMap.geoObjects.add(objectManager);

		$.ajax({
			url: src
		}).done(function(data) {
			objectManager.add(data);
		});
	}
}


