const path = require('path');
const sortCSSmq = require('sort-css-media-queries');

function em(px, base) {
	base = base ? base : 10;
	return round((px / base) + 0.0001, 4);
}
function round(value, decimals) {
	return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

module.exports = {
	plugins: [
		require('postcss-import'),
		require('postcss-mixins'),

		require('postcss-simple-vars'),
		require('postcss-nested-ancestors'),
		require('postcss-nested'),

		require('postcss-preset-env')({
			stage: 0,
			preserve: false
		}),

		require('postcss-calc')({
			precision: 5,
			mediaQueries: true
		}),
		require('postcss-url')([
			{
				url: 'inline',
				filter: /\.svg$/,
				basePath: path.resolve(__dirname, '')
			},
			{
				url: 'inline',
				filter: /\.woff$/,
				basePath: path.resolve(__dirname, '')
			}
		]),
		require('postcss-functions')({
			functions: {
				if: function (base) {
					console.log(base);
					return base;
				},
				em: function (px, base) {
					return em(px, base) + 'em';
				},
				rem: function (px, base) {
					return em(px, base) + 'rem';
				},
				_lh: function (lh, sz) {
					return em(lh, sz);
				},
				_font: function (sz, lh, nm) {
					return sz + 'px/' + em(lh, sz) + ' ' + nm;
				}
			}
		}),
		require('postcss-conditionals'),


		require('rucksack-css')({
			alias: false,
			clearFix: false,
			responsiveType: false,
			shorthandPosition: false
		}),
		require('cssnano')({
			zindex: false,
			autoprefixer: false,
			discardComments: {
				'removeAll': true
			}
		}),
		require('autoprefixer')({
			browsers: [
				'> 5%',
				'ie >= 9',
				'last 3 versions'
			]
		}),
		require('css-mqpacker')({
			sort: sortCSSmq
		})
	]
}